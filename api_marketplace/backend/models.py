from django.db import models


class Categorie(models.Model):

    name = models.CharField(max_length=30, null=False,
                            blank=False, default=None)
    created = models.DateTimeField(auto_now_add=True, null=True)
    updated = models.DateTimeField(auto_now=True, null=True)


class Vendor(models.Model):

    name = models.CharField(max_length=30, null=False,
                            blank=False, default=None)
    created = models.DateTimeField(auto_now_add=True, null=True)
    updated = models.DateTimeField(auto_now=True, null=True)


class Product(models.Model):

    name = models.CharField(max_length=30, null=False,
                            blank=False, default=None)
    description = models.TextField(null=True, blank=True, default=None)
    price = models.FloatField(blank=True, null=True, default=0.0)
    categorie = models.ForeignKey(
        Categorie, related_name='category', blank=True, null=True, on_delete=models.CASCADE)
    vendor = models.ForeignKey(
        Vendor, related_name='vendor', blank=True, null=True, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True, null=True)
    updated = models.DateTimeField(auto_now=True, null=True)
