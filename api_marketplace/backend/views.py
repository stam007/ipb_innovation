from rest_framework import viewsets
from .models import *
from backend.serializers import *
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters


class CategorieViewSet(viewsets.ModelViewSet):
    queryset = Categorie.objects.all()
    serializer_class = CategorieSerializer

    filter_backends = [DjangoFilterBackend, filters.SearchFilter]
    search_fields = ['id', 'name']
    filterset_fields = ['id', 'name']


class VendorViewSet(viewsets.ModelViewSet):
    queryset = Vendor.objects.all()
    serializer_class = VendorSerializer

    filter_backends = [DjangoFilterBackend, filters.SearchFilter]
    search_fields = ['id', 'name']
    filterset_fields = ['id', 'name']


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    filter_backends = [DjangoFilterBackend,
                       filters.SearchFilter, filters.OrderingFilter]
    ordering_fields = ['price', 'vendor', 'categorie']
    search_fields = ['name', 'description']
    filterset_fields = ['price', 'categorie', 'vendor']
