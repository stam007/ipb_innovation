from django.urls import include
from rest_framework import routers
from django.urls import re_path as url
from .views import*


router = routers.DefaultRouter()
router.register(r'product', ProductViewSet)
router.register(r'categorie', CategorieViewSet)
router.register(r'vendor', VendorViewSet)


urlpatterns = [

    url('', include(router.urls)),
]

