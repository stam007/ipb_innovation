from rest_framework import serializers
from backend.models import *


class CategorieSerializer(serializers.ModelSerializer):

    class Meta:
        model = Categorie
        fields = '__all__'


class VendorSerializer(serializers.ModelSerializer):

    class Meta:
        model = Vendor
        fields = '__all__'


class ProductSerializer(serializers.ModelSerializer):
    categorie_d = serializers.SerializerMethodField()
    vendor_d = serializers.SerializerMethodField()

    class Meta:
        model = Product
        fields = '__all__'

    def get_categorie_d(self, categorie_d):
        qs = categorie_d.categorie
        return CategorieSerializer(qs, many=False, read_only=True).data

    def get_vendor_d(self, vendor_d):
        qs = vendor_d.vendor
        return VendorSerializer(qs, many=False, read_only=True).data
